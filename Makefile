##
# 6502 Binary Ninja Plugin
#
# @file
# @version 0.1

BINJA_API_A := ../binaryninja-api/bin/libbinaryninjaapi.a

INCDIR := ../binaryninja-api
INC := -I$(INCDIR)

UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Linux)
	CC := g++
	BINJAPATH := $(HOME)/Projects/binaryninja/
else
	CC := $(shell xcrun -f clang++)
	BINJAPATH := /Applications/Binary\ Ninja.app/Contents/MacOS
endif

SRCDIR := src
BUILDDIR := build
TARGETDIR := bin

TARGETNAME := bn6502
TARGET := $(TARGETDIR)/$(TARGETNAME)

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

LIBS := -L $(BINJAPATH) -l :libbinaryninjacore.so.1
CFLAGS := -c -g -std=gnu++11 -O2 -Wall -W -fPIC -pipe $(INC)

ifeq ($(UNAME_S),Darwin)
	CFLAGS += -arch x86_64 -pipe -stdlib=libc++
endif

all: $(TARGET)

ifeq ($(UNAME_S),Linux)
$(TARGET): $(OBJECTS)
	@mkdir -p $(TARGETDIR) 
	$(CC) -shared $^ $(BINJA_API_A) $(LIBS) -Wl,-rpath=$(BINJAPATH) -o $@
else
$(TARGET): $(OBJECTS)
	@mkdir -p $(TARGETDIR)
	$(CC) $^ $(BINJA_API_A) $(LIBS) -o $@
	install_name_tool $@
endif

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	$(RM) -r $(BUILDDIR) $(TARGETDIR)

.PHONY: clean

# end
