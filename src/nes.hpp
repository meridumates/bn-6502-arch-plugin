#ifndef __NES_H_
#define __NES_H_

#include <binaryninjacore.h>
#include <binaryninjaapi.h>
#include <lowlevelilinstruction.h>

#include <string>
#include <map>
#include <array>
#include <vector>
#include <functional>

#define FLAG_COUNT 7
#define FLAG_WRITE_TYPE_COUNT 4
#define INSTR_COUNT 256
// Marks instructions with no value
#define NOVAL 0xdeadbeef
// Size of NES file header in bytes
#define NES_HEADER_SIZE 16
// ROM follows the header
#define NES_ROM_OFFSET NES_HEADER_SIZE
// IDs used when registering new Architectures and Platforms
#define NES_PLATFORM_ID 17892
#define NES_ARCH_ID 8932
// NES header magic id number (NES + MS-DOS EoF)
#define NES_HEADER_MAGIC 0x1a53454e

enum M6502Registers {
    M6502_REG_A = 1,
    M6502_REG_X,
    M6502_REG_Y,
    M6502_REG_S
};

enum M6502Flags {
    M6502_FLAG_C,
    M6502_FLAG_Z,
    M6502_FLAG_I,
    M6502_FLAG_D,
    M6502_FLAG_B,
    M6502_FLAG_V,
    M6502_FLAG_S
};

enum M6502_FLAG_TYPES {
    M6502_WT_STAR,
    M6502_WT_CZS,
    M6502_WT_ZVS,
    M6502_WT_ZS
};

enum OperandType {
    NONE,
    ABS,
    ABS_DEST,
    ABS_X,
    ABS_X_DEST,
    ABS_Y,
    ABS_Y_DEST,
    ACCUM,
    ADDR,
    IMMED,
    IND,
    IND_X,
    IND_X_DEST,
    IND_Y,
    IND_Y_DEST,
    REL,
    ZERO,
    ZERO_DEST,
    ZERO_X,
    ZERO_X_DEST,
    ZERO_Y,
    ZERO_Y_DEST
};

typedef struct M6502Instruction {
    std::string instrName;
    int operand;
    uint64_t length;
    uint64_t value;
} M6502Instruction;

static const std::array<M6502Instruction, INSTR_COUNT> Instructions{{
    {"brk", NONE, 0, NOVAL}, {"ora", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"ora", ZERO, 1, NOVAL}, {"asl", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"php", NONE, 0, NOVAL},
    {"ora", IMMED, 1, NOVAL}, {"asl@", ACCUM, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"ora", ABS, 2, NOVAL}, {"asl", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bpl", REL, 1, NOVAL},
    {"ora", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"ora", ZERO_X, 1, NOVAL}, {"asl", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"clc", NONE, 0, NOVAL},
    {"ora", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"ora", ABS_X, 2, NOVAL}, {"asl", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"jsr", ADDR, 2, NOVAL},
    {"and", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"bit", ZERO, 1, NOVAL},
    {"and", ZERO, 1, NOVAL}, {"rol", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"plp", NONE, 0, NOVAL},
    {"and", IMMED, 1, NOVAL}, {"rol@", ACCUM, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"bit", ABS, 2, NOVAL},
    {"and", ABS, 2, NOVAL}, {"rol", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bmi", REL, 1, NOVAL},
    {"and", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"and", ZERO_X, 1, NOVAL}, {"rol", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"sec", NONE, 0, NOVAL},
    {"and", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"and", ABS_X, 2, NOVAL}, {"rol", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"rti", NONE, 0, NOVAL},
    {"eor", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"eor", ZERO, 1, NOVAL}, {"lsr", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"pha", NONE, 0, NOVAL},
    {"eor", IMMED, 1, NOVAL}, {"lsr@", ACCUM, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"jmp", ADDR, 2, NOVAL},
    {"eor", ABS, 2, NOVAL}, {"lsr", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bvc", REL, 1, NOVAL},
    {"eor", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"eor", ZERO_X, 1, NOVAL}, {"lsr", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"cli", NONE, 0, NOVAL},
    {"eor", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"eor", ABS_X, 2, NOVAL}, {"lsr", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"rts", NONE, 0, NOVAL},
    {"adc", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"adc", ZERO, 1, NOVAL}, {"ror", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"pla", NONE, 0, NOVAL},
    {"adc", IMMED, 1, NOVAL}, {"ror@", ACCUM, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"jmp", IND, 2, NOVAL},
    {"adc", ABS, 2, NOVAL}, {"ror", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bvs", REL, 1, NOVAL},
    {"adc", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"adc", ZERO_X, 1, NOVAL}, {"ror", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"sei", NONE, 0, NOVAL},
    {"adc", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"adc", ABS_X, 2, NOVAL}, {"ror", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"sta", IND_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"sty", ZERO_DEST, 1, NOVAL},
    {"sta", ZERO_DEST, 1, NOVAL}, {"stx", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"dey", NONE, 0, NOVAL},
    {"", NONE, 0, NOVAL}, {"txa", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"sty", ABS_DEST, 2, NOVAL},
    {"sta", ABS_DEST, 2, NOVAL}, {"stx", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bcc", REL, 1, NOVAL},
    {"sta", IND_Y_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"sty", ZERO_X_DEST, 1, NOVAL},
    {"sta", ZERO_X_DEST, 1, NOVAL}, {"stx", ZERO_Y_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"tya", NONE, 0, NOVAL},
    {"sta", ABS_Y_DEST, 2, NOVAL}, {"txs", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"sta", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"ldy", IMMED, 1, NOVAL},
    {"lda", IND_X, 1, NOVAL}, {"ldx", IMMED, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"ldy", ZERO, 1, NOVAL},
    {"lda", ZERO, 1, NOVAL}, {"ldx", ZERO, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"tay", NONE, 0, NOVAL},
    {"lda", IMMED, 1, NOVAL}, {"tax", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"ldy", ABS, 2, NOVAL},
    {"lda", ABS, 2, NOVAL}, {"ldx", ABS, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bcs", REL, 1, NOVAL},
    {"lda", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"ldy", ZERO_X, 1, NOVAL},
    {"lda", ZERO_X, 1, NOVAL}, {"ldx", ZERO_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"clv", NONE, 0, NOVAL},
    {"lda", ABS_Y, 2, NOVAL}, {"tsx", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"ldy", ABS_X, 2, NOVAL},
    {"lda", ABS_X, 2, NOVAL}, {"ldx", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"cpy", IMMED, 1, NOVAL},
    {"cmp", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"cpy", ZERO, 1, NOVAL},
    {"cmp", ZERO, 1, NOVAL}, {"dec", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"iny", NONE, 0, NOVAL},
    {"cmp", IMMED, 1, NOVAL}, {"dex", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"cpy", ABS, 2, NOVAL},
    {"cmp", ABS, 2, NOVAL}, {"dec", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"bne", REL, 1, NOVAL},
    {"cmp", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"cmp", ZERO_X, 1, NOVAL}, {"dec", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"cld", NONE, 0, NOVAL},
    {"cmp", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"cmp", ABS_X, 2, NOVAL}, {"dec", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"cpx", IMMED, 1, NOVAL},
    {"sbc", IND_X, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"cpx", ZERO, 1, NOVAL},
    {"sbc", ZERO, 1, NOVAL}, {"inc", ZERO_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"inx", NONE, 0, NOVAL},
    {"sbc", IMMED, 1, NOVAL}, {"nop", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"cpx", ABS, 2, NOVAL},
    {"sbc", ABS, 2, NOVAL}, {"inc", ABS_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"beq", REL, 1, NOVAL},
    {"sbc", IND_Y, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"sbc", ZERO_X, 1, NOVAL}, {"inc", ZERO_X_DEST, 1, NOVAL}, {"", NONE, 0, NOVAL}, {"sed", NONE, 0, NOVAL},
    {"sbc", ABS_Y, 2, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL}, {"", NONE, 0, NOVAL},
    {"sbc", ABS_X, 2, NOVAL}, {"inc", ABS_X_DEST, 2, NOVAL}, {"", NONE, 0, NOVAL}
}};

class NESViewType : public BinaryNinja::BinaryViewType {
public:
    NESViewType(const std::string& name, const std::string& longName, int bank);
    BinaryNinja::BinaryView* Create(BinaryNinja::BinaryView* data);
    BinaryNinja::BinaryView* Parse(BinaryNinja::BinaryView* data);
    bool IsTypeValidForData(BinaryNinja::BinaryView* data);
    BinaryNinja::Ref<BinaryNinja::Settings> GetLoadSettingsForData(BinaryNinja::BinaryView* data);
private:
    int bank;
};

class NESView : public BinaryNinja::BinaryView {
public:
    NESView(const std::string typeName, BinaryNinja::FileMetadata* file, BinaryView* parentView, int bank);
    NESView(BNBinaryView* view);
    bool Init();
    bool PerformIsExecutable();
    uint64_t PerformGetEntryPoint();
private:
    std::vector<std::string> SplitSymbolStr(std::string str);
    void LoadSymbolFile(std::string path);
    const std::string typeName;
    BinaryNinja::FileMetadata* file;
    BinaryView* parentView;
    int bank;
};

class M6502 : public BinaryNinja::Architecture {
public:
    M6502();

    bool GetInstructionInfo(const uint8_t* data, uint64_t addr, size_t maxLen,
                            BinaryNinja::InstructionInfo& result);
    bool GetInstructionText(const uint8_t* data, uint64_t addr, size_t& len,
                            std::vector<BinaryNinja::InstructionTextToken>& result);
    bool GetInstructionLowLevelIL(const uint8_t* data, uint64_t addr, size_t& len,
                                  BinaryNinja::LowLevelILFunction& il);
    BinaryNinja::ExprId GetFlagWriteLowLevelIL(BNLowLevelILOperation op, size_t size,
                                  uint32_t flagWriteType, uint32_t flag,
                                  BNRegisterOrConstant* operands,
                                  size_t operandCount, BinaryNinja::LowLevelILFunction& il);

    bool IsNeverBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                    size_t len);
    bool IsInvertBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                      size_t len);
    bool IsAlwaysBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                      size_t len);
    bool IsSkipAndReturnZeroPatchAvailable(const uint8_t* data, uint64_t addr,
                                           size_t len);
    bool IsSkipAndReturnValuePatchAvailable(const uint8_t* data, uint64_t addr,
                                            size_t len);
    bool ConvertToNop(uint8_t* data, uint64_t addr, size_t len);
    bool InvertBranch(uint8_t* data, uint64_t addr, size_t len);
    bool SkipAndReturnValue(uint8_t* data, uint64_t addr, size_t len,
                            uint64_t value);

    BNEndianness GetEndianness() const;
    size_t GetAddressSize() const;

    std::vector<uint32_t> GetFlagsWrittenByFlagWriteType(uint32_t writeType);
    std::vector<uint32_t> GetFlagsRequiredForFlagCondition(BNLowLevelILFlagCondition cond, uint32_t);
    BNFlagRole GetFlagRole(uint32_t flag, uint32_t);

    size_t GetInstructionAlignment();
    size_t GetDefaultIntegerSize();
    size_t GetAddressSize();
    size_t GetMaxInstructionLength();
    uint32_t GetStackPointerRegister();

    std::vector<uint32_t> GetAllFlags();
    std::string GetFlagName(uint32_t flag);
    std::vector<uint32_t>GetAllWriteTypes();
    std::string GetFlagWriteTypeName(uint32_t writeType);

    std::vector<uint32_t> GetFullWidthRegisters();
    std::vector<uint32_t> GetAllRegisters();
    BNRegisterInfo GetRegisterInfo(uint32_t reg);
    std::vector<uint32_t> GetGlobalRegisters();
    std::string GetRegisterName(uint32_t regId);

private:
    M6502Instruction DecodeInstruction(const uint8_t* data, uint64_t addr, bool& status);
    void CondBranch(BinaryNinja::LowLevelILFunction& il, BinaryNinja::ExprId cond, int dest);
    void Jump(BinaryNinja::LowLevelILFunction& il, int dest);
    BinaryNinja::ExprId GetPValue(BinaryNinja::LowLevelILFunction& il);
    void SetPValue(BinaryNinja::LowLevelILFunction& il, BinaryNinja::ExprId value);
    BinaryNinja::ExprId RTI(BinaryNinja::LowLevelILFunction& il);
    bool NeverBranch(uint8_t* data, uint64_t addr, size_t len);
    BinaryNinja::ExprId IndirectLoad(BinaryNinja::LowLevelILFunction& il, int op);
    BinaryNinja::ExprId LoadZeroPage16(BinaryNinja::LowLevelILFunction& il, int op);
    std::vector<BinaryNinja::InstructionTextToken> GetOperandTokens(int type, uint64_t value);
    const std::array<std::string, INSTR_COUNT> InstructionNames;

    // TODO: The original design was to create a generic M6502 machine
    // instruction class that exposed functions for generating data like text
    // tokens and IL objects. These lambda maps would be used to populate those
    // functions when each M6502 instruction class was created. However, after
    // creating these maps, I noticed that the BN API provides the raw binary
    // data for each instruction instead of an instruction object of your
    // creation. The expectation appears to be that you should "decompose" the
    // raw data every time the API is called. I ended up implementing the APIs
    // using the expected design, but left these maps in place. At some point,
    // these should probably be replaced with their own functions or a single
    // function with a large switch statement.
    std::map<int, std::function<void(BinaryNinja::LowLevelILFunction&, int, BinaryNinja::ExprId& id)>> OperandILResolvers{
        {NONE, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){}},
        {ABS, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.ConstPointer(2, op));}},
        {ABS_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Const(2, op);}},
        {ABS_X, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.Add(2, il.Const(2, op), il.ZeroExtend(2, il.Register(1, M6502_REG_X))));}},
        {ABS_X_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Add(2, il.Const(2, op), il.ZeroExtend(2, il.Register(1, M6502_REG_X)));}},
        {ABS_Y, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.Add(2, il.Const(2, op), il.ZeroExtend(2, il.Register(1, M6502_REG_Y))));}},
        {ABS_Y_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Add(2, il.Const(2, op), il.ZeroExtend(2, il.Register(1, M6502_REG_Y)));}},
        {ACCUM, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Register(1, M6502_REG_A);}},
        {ADDR, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Const(2, op);}},
        {IMMED, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Const(1, op);}},
        {IND, [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = IndirectLoad(il, op);}},
        {IND_X, [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, LoadZeroPage16(il, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_X))));}},
        {IND_X_DEST, [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = LoadZeroPage16(il, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_X)));}},
        {IND_Y, [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.Add(2, LoadZeroPage16(il, il.Const(1, op)), il.Register(1, M6502_REG_Y)));}},
        {IND_Y_DEST, [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Add(2, LoadZeroPage16(il, il.Const(1, op)), il.Register(1, M6502_REG_Y));}},
        {REL, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.ConstPointer(2, op);}},
        {ZERO, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.ConstPointer(2, op));}},
        {ZERO_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.ConstPointer(2, op);}},
        {ZERO_X, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.ZeroExtend(2, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_X))));}},
        {ZERO_X_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.ZeroExtend(2, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_X)));}},
        {ZERO_Y, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Load(1, il.ZeroExtend(2, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_Y))));}},
        {ZERO_Y_DEST, [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.ZeroExtend(2, il.Add(1, il.Const(1, op), il.Register(1, M6502_REG_Y)));}}
    };
    std::map<std::string, std::function<void(BinaryNinja::LowLevelILFunction&, int, BinaryNinja::ExprId& id)>> InstructionILResolvers{
        {"adc", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.AddCarry(1, il.Register(1, M6502_REG_A), op, il.Flag(M6502_FLAG_C), M6502_WT_STAR));}},
        {"asl", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.ShiftLeft(1, il.Load(1, op), il.Const(1, 1), M6502_WT_CZS));}},
        {"asl@", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.ShiftLeft(1, op, il.Const(1, 1), M6502_WT_CZS));}},
        {"and", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.And(1, il.Register(1, M6502_REG_A), op, M6502_WT_ZS));}},
        {"bcc", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.FlagCondition(LLFC_UGE), op);}},
        {"bcs", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.FlagCondition(LLFC_ULT), op);}},
        {"beq", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.FlagCondition(LLFC_E), op);}},
        {"bit", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.And(1, il.Register(1, M6502_REG_A), op, M6502_WT_CZS);}},
        {"bmi", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.FlagCondition(LLFC_NEG), op);}},
        {"bne", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId id){CondBranch(il, il.FlagCondition(LLFC_NE), op);}},
        {"bpl", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.FlagCondition(LLFC_POS), op);}},
        {"brk", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SystemCall();}},
        {"bvc", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.Not(0, il.Flag(M6502_FLAG_V)), op);}},
        {"bvs", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){CondBranch(il, il.Flag(M6502_FLAG_V), op);}},
        {"clc", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_C, il.Const(0, 0));}},
        {"cld", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_D, il.Const(0, 0));}},
        {"cli", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_I, il.Const(0, 0));}},
        {"clv", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_V, il.Const(0, 0));}},
        {"cmp", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Sub(1, il.Register(1, M6502_REG_A), op, M6502_WT_CZS);}},
        {"cpx", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Sub(1, il.Register(1, M6502_REG_X), op, M6502_WT_CZS);}},
        {"cpy", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Sub(1, il.Register(1, M6502_REG_Y), op, M6502_WT_CZS);}},
        {"dec", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.Sub(1, il.Load(1, op), il.Const(1, 1), M6502_WT_ZS));}},
        {"dex", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_X, il.Sub(1, il.Register(1, M6502_REG_X), il.Const(1, 1), M6502_WT_ZS));}},
        {"dey", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_Y, il.Sub(1, il.Register(1, M6502_REG_Y), il.Const(1, 1), M6502_WT_ZS));}},
        {"eor", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.Xor(1, il.Register(1, M6502_REG_A), op, M6502_WT_ZS));}},
        {"inc", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.Add(1, il.Load(1, op), il.Const(1, 1), M6502_WT_ZS));}},
        {"inx", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_X, il.Add(1, il.Register(1, M6502_REG_X), il.Const(1, 1), M6502_WT_ZS));}},
        {"iny", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_Y, il.Add(1, il.Register(1, M6502_REG_Y), il.Const(1, 1), M6502_WT_ZS));}},
        {"jmp", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){Jump(il, op);}},
        {"jsr", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Call(op);}},
        {"lda", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, op, M6502_WT_ZS);}},
        {"ldx", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_X, op, M6502_WT_ZS);}},
        {"ldy", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_Y, op, M6502_WT_ZS);}},
        {"lsr", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.LogicalShiftRight(1, il.Load(1, op), il.Const(1, 1), M6502_WT_CZS));}},
        {"lsr@", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.LogicalShiftRight(1, il.Register(1, M6502_REG_A), il.Const(1, 1), M6502_WT_CZS));}},
        {"nop", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Nop();}},
        {"ora", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.Or(1, il.Register(1, M6502_REG_A), op, M6502_WT_ZS));}},
        {"pha", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Push(1, il.Register(1, M6502_REG_A));}},
        {"php", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Push(1, GetPValue(il));}},
        {"pla", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.Pop(1), M6502_WT_ZS);}},
        {"plp", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){SetPValue(il, il.Pop(1));}},
        {"rol", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.RotateLeftCarry(1, il.Load(1, op), il.Const(1, 1), il.Flag(M6502_FLAG_C), M6502_WT_CZS));}},
        {"rol@", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.RotateLeftCarry(1, il.Register(1, M6502_REG_A), il.Const(1, 1), il.Flag(M6502_FLAG_C), M6502_WT_CZS));}},
        {"ror", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.RotateRightCarry(1, il.Load(1, op), il.Const(1, 1), il.Flag(M6502_FLAG_C), M6502_WT_CZS));}},
        {"ror@", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.RotateRightCarry(1, il.Register(1, M6502_REG_A), il.Const(1, 1), il.Flag(M6502_FLAG_C), M6502_WT_CZS));}},
        {"rti", [this](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = RTI(il);}},
        {"rts", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Return(il.Add(2, il.Pop(2), il.Const(2, 1)));}},
        {"sbc", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.SubBorrow(1, il.Register(1, M6502_REG_A), op, il.Flag(M6502_FLAG_C), M6502_WT_STAR));}},
        {"sec", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_C, il.Const(0, 1));}},
        {"sed", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_D, il.Const(0, 1));}},
        {"sei", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetFlag(M6502_FLAG_I, il.Const(0, 1));}},
        {"sta", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.Register(1, M6502_REG_A));}},
        {"stx", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.Register(1, M6502_REG_X));}},
        {"sty", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.Store(1, op, il.Register(1, M6502_REG_Y));}},
        {"tax", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_X, il.Register(1, M6502_REG_A), M6502_WT_ZS);}},
        {"tay", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_Y, il.Register(1, M6502_REG_A), M6502_WT_ZS);}},
        {"tsx", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_X, il.Register(1, M6502_REG_S), M6502_WT_ZS);}},
        {"txa", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.Register(1, M6502_REG_X), M6502_WT_ZS);}},
        {"txs", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_S, il.Register(1, M6502_REG_X));}},
        {"tya", [](BinaryNinja::LowLevelILFunction& il, int op, BinaryNinja::ExprId& id){id = il.SetRegister(1, M6502_REG_A, il.Register(1, M6502_REG_Y), M6502_WT_ZS);}},
    };
};
#endif // __NES_H_
