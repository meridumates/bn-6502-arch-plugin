#include "nes.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;
using namespace BinaryNinja;

#define MYLOG LogDebug

M6502::M6502()
    : Architecture{"6502"}
    {};

vector<InstructionTextToken> M6502::GetOperandTokens(int type, uint64_t value) {
    stringstream t;
    vector<InstructionTextToken> tokens;

    switch(type) {
        case(NONE):
            break;
        case (ABS):
        case (ABS_DEST):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            break;
        case (ABS_X):
        case (ABS_X_DEST):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "x"));
            break;
        case (ABS_Y):
        case (ABS_Y_DEST):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "y"));
            break;
        case (ACCUM):
            tokens.push_back(InstructionTextToken(RegisterToken, "a"));
            break;
        case (ADDR):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            break;
        case (IMMED):
             t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(TextToken, "#"));
            tokens.push_back(InstructionTextToken(IntegerToken, t.str(), value));
            break;
        case (IND):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(TextToken, "["));
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, "]"));
            break;
        case (IND_X):
        case (IND_X_DEST):
            t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(TextToken, "["));
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "x"));
            tokens.push_back(InstructionTextToken(TextToken, "]"));
            break;
        case (IND_Y):
        case (IND_Y_DEST):
            t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(TextToken, "["));
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "y"));
            tokens.push_back(InstructionTextToken(TextToken, "]"));
            break;
        case (REL):
            t << "$" << setfill('0') << setw(4) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            break;
        case (ZERO):
        case (ZERO_DEST):
            t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            break;
        case (ZERO_X):
        case (ZERO_X_DEST):
            t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "x"));
            break;
        case (ZERO_Y):
        case (ZERO_Y_DEST):
            t << "$" << setfill('0') << setw(2) << hex << value;
            tokens.push_back(InstructionTextToken(PossibleAddressToken, t.str(), value));
            tokens.push_back(InstructionTextToken(TextToken, ", "));
            tokens.push_back(InstructionTextToken(RegisterToken, "y"));
            break;
        default:
            MYLOG("ERROR: Operand with type ID %d not supported\n", type);
    }
   
    return tokens;
};

ExprId M6502::RTI(LowLevelILFunction& il) {
    SetPValue(il, il.Pop(1));
    return il.Return(il.Pop(2));
}

ExprId M6502::GetPValue(LowLevelILFunction& il) {
    ExprId c, z, i, d, b, v, s;
    c = il.FlagBit(1, M6502_FLAG_C, 0);
    z = il.FlagBit(1, M6502_FLAG_Z, 1);
    i = il.FlagBit(1, M6502_FLAG_I, 2);
    d = il.FlagBit(1, M6502_FLAG_D, 3);
    b = il.FlagBit(1, M6502_FLAG_B, 4);
    v = il.FlagBit(1, M6502_FLAG_V, 6);
    s = il.FlagBit(1, M6502_FLAG_S, 7);
    return il.Or(1, il.Or(1, il.Or(1, il.Or(1, il.Or(1, il.Or(1, c, z), i), d), b), v), s);
}

void M6502::SetPValue(LowLevelILFunction& il, ExprId value) {
    il.AddInstruction(il.SetRegister(1, LLIL_TEMP(0), value));
    il.AddInstruction(il.SetFlag(M6502_FLAG_C, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x01))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_Z, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x02))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_I, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x04))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_D, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x08))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_B, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x10))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_V, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x40))));
    il.AddInstruction(il.SetFlag(M6502_FLAG_S, il.TestBit(1, il.Register(1, LLIL_TEMP(0)), il.Const(1, 0x80))));
    return;
}

void M6502::Jump(LowLevelILFunction& il, int dest) {
    BNLowLevelILLabel *label = nullptr;
    LowLevelILInstruction lis = il.GetInstruction(0);
    if (lis.operation == LLIL_CONST) {
        label = il.GetLabelForAddress(this, lis.GetConstant());
    }
    if (label == nullptr) {
        il.AddInstruction(il.Jump(dest));
    }
    else {
        il.AddInstruction(il.Goto(*label));
    }
    return;
}

void M6502::CondBranch(LowLevelILFunction& il, ExprId cond, int dest) {
    BNLowLevelILLabel *t = nullptr;
    BNLowLevelILLabel t1, f;
    bool indirect;
    LowLevelILInstruction lis = il.GetInstruction(0);

    if (lis.operation == LLIL_CONST) {
        t = il.GetLabelForAddress(this, lis.GetConstant());
    }

    if (t == nullptr) {
        t1 = LowLevelILLabel();
        indirect = true;
    }
    else {
        t1 = *t;
        indirect = false;
    }

    f = LowLevelILLabel();
    il.AddInstruction(il.If(cond, t1, f));
    if (indirect) {
        il.MarkLabel(t1);
        il.AddInstruction(il.Jump(dest));
    }
    il.MarkLabel(f);
    return;
}

ExprId M6502::LoadZeroPage16(LowLevelILFunction& il, int op) {
    ExprId loAddr, hiAddr, lo, hi;
    LowLevelILInstruction lis = il.GetInstruction(0);

    if (lis.operation == LLIL_CONST) {
        if (lis.GetConstant() == 0xFF) {
            lo = il.ZeroExtend(2, il.Load(1, il.ConstPointer(2, 0xFF)));
            hi = il.ShiftLeft(2, il.ZeroExtend(2, il.Load(1, il.ConstPointer(2, 0))), il.Const(2, 8));
            return il.Or(2, lo, hi);
        }
        else {
            return il.Load(2, il.ConstPointer(2, lis.GetConstant()));
        }
    }
    else {
        il.AddInstruction(il.SetRegister(1, LLIL_TEMP(0), op));
        op = il.Register(1, LLIL_TEMP(0));
        loAddr = op;
        hiAddr = il.Add(1, op, il.Const(1, 1));
        lo = il.ZeroExtend(2, il.Load(1, loAddr));
        hi = il.ShiftLeft(2, il.ZeroExtend(2, il.Load(1, hiAddr)), il.Const(2, 8));
        return il.Or(2, lo, hi);
    }

}

ExprId M6502::IndirectLoad(LowLevelILFunction& il, int op) {
    ExprId loAddr, hiAddr, lo, hi;

    if ((op & 0xFF) == 0xFF) {
        loAddr = il.ConstPointer(2, op);
        hiAddr = il.ConstPointer(2, (op & 0xFF00) | ((op + 1) & 0xFF));
        lo = il.ZeroExtend(2, il.Load(1, loAddr));
        hi = il.ShiftLeft(2, il.ZeroExtend(2, il.Load(1, hiAddr)), il.Const(2, 8));
        return il.Or(2, lo, hi);
    }
    else {
        return il.Load(2, il.ConstPointer(2, op));
    }
}

size_t M6502::GetInstructionAlignment() {
    return 1;
}

size_t M6502::GetDefaultIntegerSize() {
    return 1;
}

size_t M6502::GetAddressSize() {
    return 2;
}

size_t M6502::GetMaxInstructionLength() {
    return 3;
}

uint32_t M6502::GetStackPointerRegister() {
    return M6502_REG_S;
}

bool M6502::GetInstructionInfo(const uint8_t* data, uint64_t addr, size_t maxLen,
                            InstructionInfo& result) {
    bool success = false;
    M6502Instruction instr = DecodeInstruction(data, addr, success);

    if (!(success)) {
        return false;
    }

    result.length = instr.length;
    maxLen = instr.length;

    if (instr.instrName == "jmp") {
        if (instr.operand == ADDR) {
            result.AddBranch(UnconditionalBranch, (uint64_t)*((uint16_t*)&data[1]));
        }
        else {
            result.AddBranch(UnresolvedBranch);
        }
    }
    else if (instr.instrName == "jsr") {
        result.AddBranch(CallDestination, (uint64_t)*((uint16_t*)&data[1]));
    }
    else if ((instr.instrName == "rti") || (instr.instrName == "rts")) {
        result.AddBranch(FunctionReturn);
    }
    if ((instr.instrName == "bcc") || (instr.instrName == "bcs") ||
             (instr.instrName == "beq") || (instr.instrName == "bmi") ||
             (instr.instrName == "bne") || (instr.instrName == "bpl") ||
             (instr.instrName == "bvc") || (instr.instrName == "bvs")) {
        //MYLOG("Found a branch: %s\n", instr.instrName.c_str());
        //MYLOG("addr: %ul, data: %ul\n", addr, data[1]);
        uint64_t dest = (addr + 2 + (int8_t)data[1]) & 0xffff;
        result.AddBranch(TrueBranch, dest);
        result.AddBranch(FalseBranch, addr + 2);
    }
    //maxLen = 4;
    return true;
}

bool M6502::GetInstructionText(const uint8_t* data, uint64_t addr, size_t& len,
                            vector<InstructionTextToken>& result) {
    bool success = false;
    M6502Instruction instr = DecodeInstruction(data, addr, success);
    vector<InstructionTextToken> operands;
    ostringstream ss;

    //MYLOG("Creating disasm for %s\n", instr.instrName.c_str());

    if (!(success)) {
        return false;
    }

    len = instr.length;

    ss << instr.instrName << setw(5) << " ";
    result.emplace_back(InstructionTextToken(TextToken, ss.str()));
    operands = GetOperandTokens(instr.operand, instr.value);
    if (!operands.empty()) {
        //MYLOG("Inserting operands for %s\n", instr.instrName.c_str());
        result.insert(result.end(), operands.begin(), operands.end());
    }
    return true;
}

bool M6502::GetInstructionLowLevelIL(const uint8_t* data, uint64_t addr, size_t& len,
                                  LowLevelILFunction& il) {
    bool success = false;
    ExprId operand, instrIL = NOVAL;
    M6502Instruction instr = DecodeInstruction(data, addr, success);

    if (!(success)) {
        return false;
    }

    OperandILResolvers[instr.operand](il, instr.value, operand);
    InstructionILResolvers[instr.instrName](il, operand, instrIL);
    //MYLOG("IL: op: %lul il: %lul", operand, instrIL);
    if (instrIL != NOVAL) {
        il.AddInstruction(instrIL);
    }
    len = instr.length;
    return true;
}

vector<uint32_t> M6502::GetAllFlags() {
    return vector<uint32_t> {
        M6502_FLAG_C,
        M6502_FLAG_Z,
        M6502_FLAG_I,
        M6502_FLAG_D,
        M6502_FLAG_B,
        M6502_FLAG_V,
        M6502_FLAG_S};
}

string M6502::GetFlagName(uint32_t flag) {
    switch(flag) {
        case M6502_FLAG_C: return "c";
        case M6502_FLAG_Z: return "z";
        case M6502_FLAG_I: return "i";
        case M6502_FLAG_D: return "d";
        case M6502_FLAG_B: return "b";
        case M6502_FLAG_V: return "v";
        case M6502_FLAG_S: return "s";
        default:
            MYLOG("ERROR: Unsupported flag ID: %d\n", flag);
            return "INVALID_FLAG";
    }
}

vector<uint32_t> M6502::GetAllWriteTypes() {
    return vector<uint32_t> {
        M6502_WT_STAR,
        M6502_WT_CZS,
        M6502_WT_ZVS,
        M6502_WT_ZS};
}

string M6502::GetFlagWriteTypeName(uint32_t writeType) {
    switch(writeType) {
        case M6502_WT_STAR: return "*";
        case M6502_WT_CZS: return "czz";
        case M6502_WT_ZVS: return "zvs";
        case M6502_WT_ZS: return "zs";
        default:
            MYLOG("ERROR: Unsupported flag write type ID: %d", writeType);
            return "INVALID_FLAG_WRITE_TYPE";
    }
}

ExprId M6502::GetFlagWriteLowLevelIL(BNLowLevelILOperation op, size_t size,
                                  uint32_t flagWriteType, uint32_t flag,
                                  BNRegisterOrConstant* operands,
                                  size_t operandCount, LowLevelILFunction& il) {
    if (flag == M6502_FLAG_C) {
        if ((op == LLIL_SUB) || (op == LLIL_SBB)) {
            return il.Not(0, GetDefaultFlagWriteLowLevelIL(op, size, CarryFlagRole, operands, operandCount, il));
        }
        else {
            return GetDefaultFlagWriteLowLevelIL(op, size, CarryFlagRole, operands, operandCount, il);
        }
    }
    return Architecture::GetFlagWriteLowLevelIL(op, size, flagWriteType, flag, operands, operandCount, il);
}

bool M6502::IsNeverBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                    size_t len) {
    switch (data[0]) {
        case 0x10:
        case 0x30:
        case 0x50:
        case 0x70:
        case 0x90:
        case 0xb0:
        case 0xd0:
        case 0xf0:
            return true;
        default:
            return false;
    }
}

bool M6502::IsInvertBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                      size_t len) {
    switch (data[0]) {
        case 0x10:
        case 0x30:
        case 0x50:
        case 0x70:
        case 0x90:
        case 0xb0:
        case 0xd0:
        case 0xf0:
            return true;
        default:
            return false;
    }
}

bool M6502::IsAlwaysBranchPatchAvailable(const uint8_t* data, uint64_t addr,
                                      size_t len) {
    return false;
}

bool M6502::IsSkipAndReturnZeroPatchAvailable(const uint8_t* data, uint64_t addr,
                                           size_t len) {
    if ((data[0] == 0x20) && (len == 3)) {
        return true;
    }
    else {
        return false;
    }
}

bool M6502::IsSkipAndReturnValuePatchAvailable(const uint8_t* data, uint64_t addr,
                                            size_t len) {
    if ((data[0] == 0x20) && (len == 3)) {
        return true;
    }
    else {
        return false;
    }
}

bool M6502::ConvertToNop(uint8_t* data, uint64_t addr, size_t len) {
    if (len < 1) {
        return false;
    }

    for(int i = 0; i < (int)len; i++) {
        data[i] = 0xea;
    }
    return true;
}

bool M6502::InvertBranch(uint8_t* data, uint64_t addr, size_t len) {
    switch (data[0]) {
        case 0x10:
        case 0x30:
        case 0x50:
        case 0x70:
        case 0x90:
        case 0xb0:
        case 0xd0:
        case 0xf0:
            data[0] ^= 0x20;
            return true;
        default:
            return false;
    }
}

bool M6502::NeverBranch(uint8_t* data, uint64_t addr, size_t len) {
    switch (data[0]) {
        case 0x10:
        case 0x30:
        case 0x50:
        case 0x70:
        case 0x90:
        case 0xb0:
        case 0xd0:
        case 0xf0:
            return ConvertToNop(data, addr, len);
        default:
            return false;
    }
}

bool M6502::SkipAndReturnValue(uint8_t* data, uint64_t addr, size_t len,
                            uint64_t value) {
   if ((data[0] != 0x20) || len != 3) {
       return false;
   }
   else {
       data[0] = 0xa9;
       data[1] = value & 0xff;
       data[2] = 0xea;
       return true;
   }
}

BNEndianness M6502::GetEndianness() const {
    return LittleEndian;
}

size_t M6502::GetAddressSize() const {
    return 2;
}

M6502Instruction M6502::DecodeInstruction(const uint8_t* data, uint64_t addr, bool& status) {
    M6502Instruction instr = {};
    int opcode = 0;
    status = false;

    if (sizeof(data) < 1) {
        return instr;
    }

    opcode = data[0];
    instr = Instructions[opcode];

    if (instr.instrName.empty()) {
        return instr;
    }

    if (instr.length == 0) {
        instr.value = -1;
    }
    else if (instr.operand == REL) {
        instr.value = (addr + 2 + (int8_t)data[1]) & 0xFFFF;
    }
    else if (instr.length == 1) {
        instr.value = data[1];
    }
    else {
        instr.value = (int)*((uint16_t*)&data[1]);
    }

    //MYLOG("instrName: %s len: %ul addr: 0x%ul, val: %ul", instr.instrName.c_str(), instr.length, addr, instr.value);

    instr.length++;
    status = true;
    return instr;
}

BNRegisterInfo M6502::GetRegisterInfo(uint32_t reg) {
    switch(reg) {
        case M6502_REG_A: return {M6502_REG_A, 0, 1, NoExtend};
        case M6502_REG_X: return {M6502_REG_X, 0, 1, NoExtend};
        case M6502_REG_Y: return {M6502_REG_Y, 0, 1, NoExtend};
        case M6502_REG_S: return {M6502_REG_S, 0, 1, NoExtend};
        default: return {0, 0, 0, NoExtend};
    }
}

vector<uint32_t> M6502::GetFullWidthRegisters() {
    return vector<uint32_t> {
        M6502_REG_A,
        M6502_REG_X,
        M6502_REG_Y,
        M6502_REG_S};
}

vector<uint32_t> M6502::GetAllRegisters() {
    return vector<uint32_t> {
        M6502_REG_A,
        M6502_REG_X,
        M6502_REG_Y,
        M6502_REG_S};
}

vector<uint32_t> M6502::GetGlobalRegisters() {
    return vector<uint32_t> {
        M6502_REG_A,
        M6502_REG_X,
        M6502_REG_Y,
        M6502_REG_S};
}

string M6502::GetRegisterName(uint32_t regId) {
    switch(regId) {
        case M6502_REG_A: return "a";
        case M6502_REG_X: return "x";
        case M6502_REG_Y: return "y";
        case M6502_REG_S: return "s";
        default:
            MYLOG("ERROR: Unsupported register ID: %d\n", regId);
            return "INVALID_REG";
    }
}

vector<uint32_t> M6502::GetFlagsWrittenByFlagWriteType(uint32_t writeType) {
    switch (writeType) {
        case M6502_WT_STAR:
            return vector<uint32_t> {M6502_FLAG_C, M6502_FLAG_Z, M6502_FLAG_V, M6502_FLAG_S};
        case M6502_WT_CZS:
            return vector<uint32_t> {M6502_FLAG_C, M6502_FLAG_Z, M6502_FLAG_S};
        case M6502_WT_ZVS:
            return vector<uint32_t> {M6502_FLAG_Z, M6502_FLAG_V, M6502_FLAG_S};
        case M6502_WT_ZS:
            return vector<uint32_t> {M6502_FLAG_Z, M6502_FLAG_S};
        default:
            return vector<uint32_t>();
    }
}

vector<uint32_t>
M6502::GetFlagsRequiredForFlagCondition(BNLowLevelILFlagCondition cond, uint32_t) {
    switch(cond) {
        case LLFC_UGE:
        case LLFC_ULT:
            return vector<uint32_t> {M6502_FLAG_C};
        case LLFC_E:
        case LLFC_NE:
            return vector<uint32_t> {M6502_FLAG_Z};
        case LLFC_NEG:
        case LLFC_POS:
            return vector<uint32_t> {M6502_FLAG_S};
        default:
            return vector<uint32_t>();
    }
}

BNFlagRole M6502::GetFlagRole(uint32_t flag, uint32_t) {
    switch (flag) {
        case M6502_FLAG_C: return SpecialFlagRole;
        case M6502_FLAG_Z: return ZeroFlagRole;
        case M6502_FLAG_V: return OverflowFlagRole;
        case M6502_FLAG_S: return NegativeSignFlagRole;
        //TODO: better error handling - terminate vs log and continue, sending
        //SpecialFlagRole as default right now
        default:
            MYLOG("ERROR: Flag ID: %d is unsupported\n", flag);
            return SpecialFlagRole;
    }
}

NESView::NESView(BNBinaryView* view)
    : BinaryView{view}
{
    return;
}

NESView::NESView(const string typeName, FileMetadata* file, BinaryView* parentView, int bank)
    : BinaryView{typeName, file, parentView}
    , typeName(typeName)
    , file(file)
    , parentView(parentView)
    , bank(bank)
{
    return;
}

vector<string> NESView::SplitSymbolStr(string str) {
    vector<string> tokens;
    string delim = "#";
    stringstream src(str);
    string tmp;

    while(getline(src, tmp, '#')) {
        tokens.push_back(tmp);
    }

    return tokens;
}

bool NESView::Init() {
    int romOffset = NES_ROM_OFFSET;
    int romLen, romBanks;
    char header[NES_HEADER_SIZE];
    uint16_t nmi, start, irq = 0;
    stringstream symbolFileRAM, symbolFileROM, symbolFileBank;

    size_t bRead = parentView->Read(header, 0, sizeof(header));
    if (bRead != sizeof(header)) {
        MYLOG("ERROR: failed to read full header\n");
        return false;
    }

    romBanks = header[4];
    romLen = romBanks * 0x4000;

    AddAutoSegment(0, 0x8000, 0, 0, SegmentReadable | SegmentWritable | SegmentExecutable);
    AddAutoSegment(0x8000, 0x4000, romOffset + (bank * 0x4000), 0x4000, SegmentReadable | SegmentExecutable);
    AddAutoSegment(0xc000, 0x4000, romOffset + romLen - 0x4000, 0x4000, SegmentReadable | SegmentExecutable);

    bRead = Read(&nmi, 0xfffa, sizeof(nmi));
    bRead = Read(&start, 0xfffc, sizeof(start));
    bRead = Read(&irq, 0xfffe, sizeof(irq));

    //MYLOG("nmi: %ul, start: %ul, irq: %ul", nmi, start, irq);

    DefineAutoSymbol(new Symbol(FunctionSymbol, "_nmi", nmi));
    AddFunctionForAnalysis(GetDefaultPlatform(), nmi);

    DefineAutoSymbol(new Symbol(FunctionSymbol, "_start", start));
    AddFunctionForAnalysis(GetDefaultPlatform(), start);

    DefineAutoSymbol(new Symbol(FunctionSymbol, "_irq", irq));
    AddFunctionForAnalysis(GetDefaultPlatform(), irq);

    AddEntryPointForAnalysis(GetDefaultPlatform(), start);

    // Hardware Registers
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUCTRL", 0x2000));
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUMASK", 0x2001));
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUSTATUS", 0x2002));
    DefineAutoSymbol(new Symbol(DataSymbol, "OAMADDR", 0x2003));
    DefineAutoSymbol(new Symbol(DataSymbol, "OAMDATA", 0x2004));
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUSCROLL", 0x2005));
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUADDR", 0x2006));
    DefineAutoSymbol(new Symbol(DataSymbol, "PPUDATA", 0x2007));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ1_VOL", 0x4000));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ1_SWEEP", 0x4001));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ1_LO", 0x4002));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ1_HI", 0x4003));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ2_VOL", 0x4004));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ2_SWEEP", 0x4005));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ2_LO", 0x4006));
    DefineAutoSymbol(new Symbol(DataSymbol, "SQ2_HI", 0x4007));
    DefineAutoSymbol(new Symbol(DataSymbol, "TRI_LINEAR", 0x4008));
    DefineAutoSymbol(new Symbol(DataSymbol, "TRI_LO", 0x400a));
    DefineAutoSymbol(new Symbol(DataSymbol, "TRI_HI", 0x400b));
    DefineAutoSymbol(new Symbol(DataSymbol, "NOISE_VOL", 0x400c));
    DefineAutoSymbol(new Symbol(DataSymbol, "NOISE_LO", 0x400e));
    DefineAutoSymbol(new Symbol(DataSymbol, "NOISE_HI", 0x400f));
    DefineAutoSymbol(new Symbol(DataSymbol, "DMC_FREQ", 0x4010));
    DefineAutoSymbol(new Symbol(DataSymbol, "DMC_RAW", 0x4011));
    DefineAutoSymbol(new Symbol(DataSymbol, "DMC_START", 0x4012));
    DefineAutoSymbol(new Symbol(DataSymbol, "DMC_LEN", 0x4013));
    DefineAutoSymbol(new Symbol(DataSymbol, "OAMDMA", 0x4014));
    DefineAutoSymbol(new Symbol(DataSymbol, "SND_CHN", 0x4015));
    DefineAutoSymbol(new Symbol(DataSymbol, "JOY1", 0x4016));
    DefineAutoSymbol(new Symbol(DataSymbol, "JOY2", 0x4017));

    symbolFileBank << file->GetFilename() << "." << hex << bank << ".nl";
    symbolFileROM << file->GetFilename() << "." << hex << (romBanks - 1) << ".nl";
    symbolFileRAM << file->GetFilename() << ".ram.nl";

    LoadSymbolFile(symbolFileBank.str());
    LoadSymbolFile(symbolFileROM.str());
    LoadSymbolFile(symbolFileRAM.str());
   
    return true;
}

void NESView::LoadSymbolFile(string path) {
    string tmpStr, funcName;
    uint16_t addr;
    vector<string> symbols;

    //MYLOG("Opening symbol file: %s\n", path.c_str());
    ifstream fin(path);
    if (!fin.good()) {
        return;
    }

    while(getline(fin, tmpStr)) {
        if(tmpStr.size() < 1) {
            continue;
        }

        symbols = SplitSymbolStr(tmpStr);
        if (symbols.size() > 3) {
            continue;
        }

        // Remove the "$" that precedes the address
        addr = stoul(symbols[0].erase(0, 1), 0, 16);
        funcName = symbols[1];
        //MYLOG("Found symbol for %s at %ul\n", funcName.c_str(), addr);

        DefineAutoSymbol(new Symbol(FunctionSymbol, funcName, addr));
        if (addr >= 0x8000) {
            AddFunctionForAnalysis(GetDefaultPlatform(), addr);
        }
    }
}

bool NESView::PerformIsExecutable() {
    return true;
}

uint64_t NESView::PerformGetEntryPoint() {
    uint64_t addr = 0;
    PerformRead(&addr, 0xfffc, 2);
    return addr;
}


NESViewType::NESViewType(const string& name, const string& longName, int bank)
    : BinaryViewType{name, longName}
    , bank(bank)
{
    return;
}

BinaryView* NESViewType::Create(BinaryView* data) {
    NESView * nView = new NESView("NES", data->GetFile(), data, bank);
    nView->SetDefaultPlatform(GetPlatform(NES_PLATFORM_ID, GetArchitecture(NES_ARCH_ID, LittleEndian)));
    return nView;
}

BinaryView* NESViewType::Parse(BinaryView* data) {
    NESView * nView = new NESView("NES", data->GetFile(), data, bank);
    nView->SetDefaultPlatform(GetPlatform(NES_PLATFORM_ID, GetArchitecture(NES_ARCH_ID, LittleEndian)));
    return nView;
}

bool NESViewType::IsTypeValidForData(BinaryView* data) {
    size_t bRead = 0;
    char header [NES_HEADER_SIZE];
    uint8_t romBanks = 0;

    bRead = data->Read(&header, 0, sizeof(header));
    if (bRead != sizeof(header)) {
        return false;
    }

    if (*((uint32_t*)&header[0]) != NES_HEADER_MAGIC) {
        return false;
    }

    romBanks = header[4];
    if (romBanks < (bank + 1)) {
        return false;
    }

    return true;
}

Ref<Settings> NESViewType::GetLoadSettingsForData(BinaryView* data) {
    MYLOG("Loading settings\n");
    BNSettings* settings = BNGetBinaryViewLoadSettingsForData(m_object, data->GetObject());
	if (!settings)
		return nullptr;
    return new Settings(settings);
}



extern "C"
{
    BINARYNINJAPLUGIN bool CorePluginInit() {
        MYLOG("M6502 Plugin Loaded\n");
        Architecture* arch = new M6502();
        Architecture::Register(arch);
        stringstream sName, lName;
        vector<NESViewType*> store;
        NESViewType* nesViewType;

        for(int i = 0; i < 32; i++) {
            sName.str(string());
            sName << "NES Bank " << uppercase << hex << i;
            lName.str(string());
            lName << "NES ROM (bank " << uppercase << hex << i << ")";
            nesViewType = new NESViewType(sName.str(), lName.str(), i);
            BinaryViewType::Register(nesViewType);
            nesViewType->RegisterArchitecture(NES_ARCH_ID, LittleEndian, arch);
            nesViewType->RegisterPlatform(NES_PLATFORM_ID, arch, arch->GetStandalonePlatform());
            store.push_back(nesViewType);
        }
        return true;
    }
}

/*int main(int argc, char *argv[]) {
    cout << "Starting 6502 BN Plugin" << endl;
}*/
